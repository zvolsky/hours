from django.contrib import admin

# Register your models here.
from myhours.models import Project, Price, Hours


class HoursMA(admin.ModelAdmin):
    def get_changeform_initial_data(self, request):
        get_data = super().get_changeform_initial_data(request)
        get_data['auth_user'] = request.user.id
        return get_data

admin.site.register(Project, admin.ModelAdmin)
admin.site.register(Price, admin.ModelAdmin)
admin.site.register(Hours, HoursMA)
