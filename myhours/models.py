from django.conf import settings
from django.db import models
from django.utils import timezone


# Create your models here.
class Project(models.Model):
    class Meta:
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'

    def __str__(self):
        return self.name

    name = models.CharField(max_length=50)

class Price(models.Model):
    class Meta:
        verbose_name = 'Price'
        verbose_name_plural = 'Prices'

    def __str__(self):
        return '{name} {price}'.format(name=self.name, price=self.price)

    project = models.ForeignKey(Project)  # db_column='id' ; _id se přidá automaticky ; může taky mít default, jinak se zadá při makemigrations
    is_default = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    name = models.CharField(max_length=50, default='basic')

class Hours(models.Model):
    class Meta:
        verbose_name = 'Hours'
        verbose_name_plural = 'Hours'

    def __str__(self):
        return '{project} {minutes}'.format(project=self.project, minutes=self.duration)

    auth_user = models.ForeignKey(settings.AUTH_USER_MODEL)
    project = models.ForeignKey(Project)
    when = models.DateTimeField(default=timezone.now)
    duration = models.DurationField()
    price = models.ForeignKey(Price)
