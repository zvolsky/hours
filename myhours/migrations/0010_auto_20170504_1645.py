# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-04 16:45
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('myhours', '0009_auto_20170504_1641'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hours',
            name='when',
            field=models.DateTimeField(default=datetime.datetime(2017, 5, 4, 16, 45, 21, 692228, tzinfo=utc)),
        ),
    ]
