# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-04 16:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('myhours', '0008_auto_20170504_1640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hours',
            name='when',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
