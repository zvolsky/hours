# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-13 08:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myhours', '0003_remove_hours_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hours',
            old_name='project_id',
            new_name='project',
        ),
    ]
